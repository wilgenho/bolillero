package ma.bingo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BolilleroApplication {

	public static void main(String[] args) {
		SpringApplication.run(BolilleroApplication.class, args);
	}

}
