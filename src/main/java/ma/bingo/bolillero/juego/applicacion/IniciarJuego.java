package ma.bingo.bolillero.juego.applicacion;

import lombok.extern.log4j.Log4j2;
import ma.bingo.bolillero.juego.domain.Juego;
import ma.bingo.bolillero.juego.domain.JuegoRepository;
import ma.bingo.shared.domain.EventBus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class IniciarJuego {

    private JuegoRepository juegoRepository;
    private EventBus eventBus;

    public IniciarJuego(JuegoRepository juegoRepository, EventBus eventBus) {
        this.juegoRepository = juegoRepository;
        this.eventBus = eventBus;
    }

    public void iniciar(int cantidaBolillas) {
        Juego juego = Juego.iniciar(cantidaBolillas);
        juegoRepository.guardar(juego);
        eventBus.publish(juego.pullDomainEvents());
        log.info("Se inicia el juego con {} bolillas", cantidaBolillas);
    }

}
