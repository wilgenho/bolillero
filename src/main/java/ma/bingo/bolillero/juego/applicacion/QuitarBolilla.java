package ma.bingo.bolillero.juego.applicacion;

import lombok.extern.log4j.Log4j2;
import ma.bingo.bolillero.juego.domain.Juego;
import ma.bingo.bolillero.juego.domain.JuegoRepository;
import ma.bingo.shared.domain.EventBus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class QuitarBolilla {

    private JuegoRepository juegoRepository;
    private EventBus eventBus;

    public QuitarBolilla(JuegoRepository juegoRepository, EventBus eventBus) {
        this.juegoRepository = juegoRepository;
        this.eventBus = eventBus;
    }

    public void siguiente() {
        Juego juego = juegoRepository.consultar();
        if (!juego.isFinalizado()) {
            int bolilla = juego.siguienteBolilla();
            eventBus.publish(juego.pullDomainEvents());
            log.info("Siguiente bolilla {}" , bolilla);
        } else {
            log.info("El juego está finalizado");
        }
    }
}
