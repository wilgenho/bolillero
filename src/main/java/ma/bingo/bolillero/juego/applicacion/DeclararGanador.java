package ma.bingo.bolillero.juego.applicacion;

import lombok.extern.log4j.Log4j2;
import ma.bingo.bolillero.juego.domain.Juego;
import ma.bingo.bolillero.juego.domain.JuegoRepository;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DeclararGanador {

    private JuegoRepository juegoRepository;

    public DeclararGanador(JuegoRepository juegoRepository) {
        this.juegoRepository = juegoRepository;
    }

    public void setGanador(String nombre) {
        Juego juego = juegoRepository.consultar();
        if (juego.getGanador().isEmpty()) {
            juego.setGanador(nombre);
            log.info("El ganador es {}", nombre);
        }
    }
}
