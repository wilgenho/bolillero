package ma.bingo.bolillero.juego.suscriber;

import lombok.extern.log4j.Log4j2;
import ma.bingo.bolillero.juego.applicacion.DeclararGanador;
import ma.bingo.bolillero.juego.domain.Bingo;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class BingoSuscriber {

    DeclararGanador declararGanador;

    public BingoSuscriber(DeclararGanador declararGanador) {
        this.declararGanador = declararGanador;
    }

    @RabbitListener(queues = "ganador-bingo")
    public void onBingo(Bingo bingo) {
        log.info("Bingo!!!");
        declararGanador.setGanador(bingo.getJugador());
    }

}
