package ma.bingo.bolillero.juego.web;

import ma.bingo.bolillero.juego.applicacion.IniciarJuego;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NuevoJuegoController {

    private IniciarJuego iniciarJuego;

    public NuevoJuegoController(IniciarJuego iniciarJuego) {
        this.iniciarJuego = iniciarJuego;
    }

    @GetMapping("/juego")
    public String iniciar() {
        iniciarJuego.iniciar(20);
        return "ok";
    }

}
