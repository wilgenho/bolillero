package ma.bingo.bolillero.juego.web;

import ma.bingo.bolillero.juego.applicacion.QuitarBolilla;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QuitarBolillaController {

    public QuitarBolilla quitarBolilla;

    public QuitarBolillaController(QuitarBolilla quitarBolilla) {
        this.quitarBolilla = quitarBolilla;
    }

    @GetMapping("/bolilla")
    public void siguiente() {
        quitarBolilla.siguiente();
    }
}
