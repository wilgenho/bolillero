package ma.bingo.bolillero.juego.domain;

import ma.bingo.shared.domain.DomainEvent;

public class Bingo extends DomainEvent {

    public static final String EVENTO_ORIGEN = "bolillero";
    public static final String EVENTO_NOMBRE = "juego.ganado";

    private String jugador;

    public Bingo() {
        super(EVENTO_ORIGEN, EVENTO_NOMBRE);
    }

    public Bingo(String jugador) {
        super(EVENTO_ORIGEN, EVENTO_NOMBRE);
        this.jugador = jugador;
    }

    public String getJugador() {
        return jugador;
    }

    public void setJugador(String jugador) {
        this.jugador = jugador;
    }
}
