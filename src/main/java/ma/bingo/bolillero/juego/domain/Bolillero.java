package ma.bingo.bolillero.juego.domain;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Bolillero {

    private List<Integer> bolillas;

    public Bolillero(int cantidad) {
        if (cantidad < 1) {
            throw new IllegalArgumentException("La cantidad de bolillas no debe ser menor que 1");
        }

        bolillas = IntStream.range(1, cantidad + 1).boxed().collect(Collectors.toList());
        Collections.shuffle(bolillas);
    }

    public int siguiente() {
        if (bolillas == null || bolillas.size() == 0) {
            return 0;
        }
        int bolilla = bolillas.get(0);
        bolillas.remove(0);
        return bolilla;
    }

    public int cantidad() {
        return bolillas.size();
    }

}
