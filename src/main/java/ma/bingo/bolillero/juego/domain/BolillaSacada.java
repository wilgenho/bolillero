package ma.bingo.bolillero.juego.domain;

import ma.bingo.shared.domain.DomainEvent;

public class BolillaSacada extends DomainEvent {

    public static final String EVENTO_ORIGEN = "bolillero";
    public static final String EVENTO_NOMBRE = "bolilla.quitada";

    private int bolilla;

    public BolillaSacada() {
        super(EVENTO_ORIGEN, EVENTO_NOMBRE);
    }

    public BolillaSacada(int bolilla) {
        super(EVENTO_ORIGEN, EVENTO_NOMBRE);
        this.bolilla = bolilla;
    }

    public int getBolilla() {
        return bolilla;
    }

    public void setBolilla(int bolilla) {
        this.bolilla = bolilla;
    }
}
