package ma.bingo.bolillero.juego.domain;

public class Jugador {

    private String nombre;

    public Jugador(String nombre) {
        if (nombre == null || nombre.isEmpty()) {
            throw new IllegalArgumentException("Nombre no puede estar vacio");
        }
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
}
