package ma.bingo.bolillero.juego.domain;

import ma.bingo.shared.domain.AggregateRoot;

public class Juego extends AggregateRoot {

    private Bolillero bolillero;
    private int ultimaBolilla;
    private boolean finalizado;
    private Jugador ganador;

    private Juego(int cantidad) {
        finalizado = false;
        ultimaBolilla = 0;
        bolillero = new Bolillero(cantidad);
        ganador = null;
    }

    public static Juego iniciar(int cantidad) {
        Juego juego = new Juego(cantidad);
        juego.record(new JuegoIniciado());
        return juego;
    }

    public int siguienteBolilla() {
        ultimaBolilla = bolillero.siguiente();
        if (bolillero.cantidad() == 0) {
            finalizado = true;
        }
        if (ultimaBolilla != 0) {
            this.record(new BolillaSacada(ultimaBolilla));
        }
        return ultimaBolilla;
    }

    public void setGanador(String nombre) {
        asegurarseSinGanador();
        asegurarseHayBolilla();
        finalizado = true;
        ganador = new Jugador(nombre);
    }

    private void asegurarseSinGanador() {
        if (ganador != null) {
            throw  new IllegalStateException("Ya existe un ganador");
        }
    }

    private void asegurarseHayBolilla() {
        if (ultimaBolilla == 0) {
            throw  new IllegalStateException("Todavía no se sacó una bolilla");
        }
    }

    public int getUltimaBolilla() {
        return ultimaBolilla;
    }

    public boolean isFinalizado() {
        return finalizado;
    }

    public String getGanador() {
        return ganador == null ? "": ganador.getNombre();
    }
}
