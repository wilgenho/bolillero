package ma.bingo.bolillero.juego.domain;

public interface JuegoRepository {

    void guardar(Juego juego);
    Juego consultar();

}
