package ma.bingo.bolillero.juego.infra;

import ma.bingo.bolillero.juego.domain.Juego;
import ma.bingo.bolillero.juego.domain.JuegoRepository;
import org.springframework.stereotype.Repository;

@Repository
public class JuegoRepositoryImpl implements JuegoRepository {

    private Juego juego;

    @Override
    public void guardar(Juego juego) {
        this.juego = juego;
    }

    @Override
    public Juego consultar() {
        return juego;
    }
}
