package ma.bingo.shared.infra;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collection;

import static org.springframework.amqp.core.Binding.DestinationType.QUEUE;

@Configuration
public class RabbitMQConfig {

    static final String EXCHANGE_BOLILLERO = "bolillero";
    static final String QUEUE_BINGO = "ganador-bingo";
    static final String TOPIC_BINGO = "juego.ganado";


    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory cf) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(cf);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }


    @Bean
    public Declarables bindings() {
        Collection<Declarable> list = new ArrayList<>();

        list.add(new Queue(QUEUE_BINGO, true, false, false));
        list.add(new Binding(QUEUE_BINGO, QUEUE, EXCHANGE_BOLILLERO, TOPIC_BINGO, null));
        return new Declarables(list);
    }

    @Bean
    public TopicExchange bolilleroExchange() {
        return new TopicExchange(EXCHANGE_BOLILLERO);
    }


}
