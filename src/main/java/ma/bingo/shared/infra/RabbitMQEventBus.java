package ma.bingo.shared.infra;

import ma.bingo.shared.domain.DomainEvent;
import ma.bingo.shared.domain.EventBus;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RabbitMQEventBus implements EventBus {

    RabbitTemplate rabbitTemplate;

    public RabbitMQEventBus(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void publish(List<DomainEvent> events) {
        events.forEach(event -> rabbitTemplate.convertAndSend(event.getOrigen(), event.getEvento(), event));
    }
}
